# workshop-proceedings


This repository contains all the proceedings (papers and presentations) of the past workshops organized or co-organized by the ICA commission on map generalisation (now ICA commission on multi-scale cartography)


## Authors and acknowledgment
This project is monitored by the ICA Commission on multi-scale cartography.

## License
All the papers and presentations collected in this project are under the CC-BY license.
